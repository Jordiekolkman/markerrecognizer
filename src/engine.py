import numpy as np
import cv2
import pymouse
import pynput
import pylab
import matplotlib

refPt = []
cropping = False

#for controlling the camera's
def Camera():
    cap = cv2.VideoCapture(1)

    for i in range(0,10):
        print cap.get(i)
    return cap

#for the filter of the image or movie
def FilterIM(IM):
    #conversion to hsv value for increased filter precision
    hsv = cv2.cvtColor(IM, cv2.COLOR_BGR2HSV)
    #upper and lower bound masking values, later this will need IR filtering
    lower_blue = np.array([90,70,80])
    upper_blue = np.array([150,150,210])
    lower_red = np.array([0,0,100])
    upper_red = np.array([100,100,255])
    lower_IR = np.array([0,0,245])
    upper_IR = np.array([0,0,255])
    #another idea here is to apply background filtering
    
    #creating the mask/filtered image
    mask = cv2.inRange(IM,lower_red,upper_red)
    #here the contour points are chosen
    ret,thresh = cv2.threshold(mask,127,255,0)
    #im2, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    
    return thresh#, contours

def BlobDetector(IM):
    j = 0
    x = []
    y = []
    IM=cv2.bitwise_not(IM)
    params = cv2.SimpleBlobDetector_Params()
    #these param settings are needed to make the detector robust enough
    #especially convexity is needed
    params.minThreshold = 1
    params.maxThreshold = 200
    params.minCircularity = 0
    params.minConvexity = 0
    detector = cv2.SimpleBlobDetector_create(params)
    keypoints = detector.detect(IM)
    #know how many blob points should be saved
    i = len(keypoints)
    if keypoints != []:
        for j in range(0,i):
            x.append(keypoints[j].pt[0]) #i is the index of the blob you want to get the position
            y.append(keypoints[j].pt[1])
    else:
        x = 0
        y = 0
    
    #makes the images with red circles around them, maybe not needed later or refined
    blobim = cv2.drawKeypoints(IM, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    return blobim, i, x, y

def MouseInput(i):
    #ginput causes several warnings, why?
    point = pylab.ginput(i)    
    return point

def PointCoupling(ClkMtx,BlbMtx):
    #pos has to be filled for a pre-defined size and then replaced with value
    pos = []
    for mincombs in range(0,min(len(ClkMtx),len(BlbMtx))):
        pos.insert(mincombs,0)
    distance = np.empty([len(ClkMtx),len(BlbMtx)])
    #see matlab minimal choice method for the coupling of mouse click to contour
    for imarkers in range (0,len(ClkMtx)):
        for iblob in range (0,len(BlbMtx)):
            disx = ClkMtx[imarkers][0] - BlbMtx[iblob][0]
            disy = ClkMtx[imarkers][1] - BlbMtx[iblob][1]
            disxy = (disx**2 + disy**2) ** (0.5)
            distance[imarkers,iblob] = disxy
    for mincombs in range(0,min(len(ClkMtx),len(BlbMtx))):
        ind = np.unravel_index(np.argmin(distance, axis=None), distance.shape)
        #an extra step in case distances are exactly equal might be needed later
        #next a statement to see if the prediction is precise enough might be implimented
        
        #a matrix of all positions should be stored here with the values from BlbMtx
        #pos.insert(ind[0],BlbMtx[ind[1]])
        pos[ind[0]] = BlbMtx[ind[1]]
        #ugly way but None or inf dont seem to work
        distance[ind] = 10000       
    #next the minimum combination has to be coupled and that entry deleted
    return pos

def BuildFullPos(pos, fullpos):
    if fullpos == []:
        fullpos = np.array(pos)
        fullpos = np.atleast_3d(fullpos)
    else:
        pos = np.atleast_3d(np.array(pos))
        fullpos = np.dstack((fullpos,pos))
    return fullpos

def PointPrediction():
    #use per frame with longer history to implement acceleration and speed if needed
    return

#loaded in should be pos for camera1 and pos for camera2
def Calculation(pos,CamTranslation):
    #still all theoretical
    imdis = pixelwidth/tandegrees(camerafov)
    angle = tandegrees((pos(x coordinate)-pixelwidth)/imdis)
    
    x = (CamTranslation(x)+CamTranslation(y)*tandegrees(angle(Cam2))) / 1-(1/tandegrees(angle(Cam1)))
    y = x*tandegrees(angle(Cam1))
    return
    

#some points for attention:
#loading in video is different then in matlab, frame info is needed most of all
#to display etc now a while loop is used, preferably this will go another way later

#https://stackoverflow.com/questions/25359288/how-to-know-total-number-of-frame-in-a-file-with-cv2-in-python
#for the information of videos loaded in

#https://stackoverflow.com/questions/39083360/why-cant-i-do-blob-detection-on-this-binary-image
#for blob detection

#https://stackoverflow.com/questions/11420748/setting-camera-parameters-in-opencv-python
#for indices to get properties of cap.get
