#this project is used to check how to load in videos, images and webcam feeds
#with correct parameters and how to optimally filter and do calculations on them

#imports
import numpy as np
import cv2
import time
import pynput
import matplotlib
import engineresearch as engine
import Tkinter
import scipy.io

#loading of video
cap = cv2.VideoCapture('D:\Downloads\zomerbaantje\start project\ImageResearch\VID_20180312_143546.avi')
#a parameter so the input only has to be given once
InputDone = False
fullpos = []

while(True):
    #reads the data from the webcam/video
    ret,IM = cap.read()
    #this creates a window with pre-determined size to be able to work with it
    cv2.resizeWindow('IM', 360,640)
    cv2.resizeWindow('mask', 360,640)
    #resizing the image for display purposes
    IMd = cv2.resize(IM,(360,640))
    mask = engine.FilterIM(IMd)
    blobim, i, xblob, yblob = engine.BlobDetector(mask)
    BlbMtx = np.column_stack((xblob,yblob))
    if ret == True:
        cv2.imshow('IM',IMd)
        cv2.imshow('mask',mask)
        cv2.imshow('blobim',blobim)   
    if InputDone == False:
        #set the plot to a certain frame
        clickx = []
        clicky = []
        matplotlib.pyplot.imshow(mask, cmap='Greys',  interpolation='nearest')
        point = engine.MouseInput(i)
        InputDone = True
        for j in range(0,i):
            clickx.append(point[j][0])
            clicky.append(point[j][1])
        #destroy figure
        matplotlib.pyplot.close()
        ClkMtx = np.column_stack((clickx,clicky))
        #makes a matrix in the following structure:
        #[x1, y1]
        #[x2, y2]... etc
    #mindistance method (point coupling)
    pos = engine.PointCoupling(ClkMtx,BlbMtx)
    fullpos = engine.BuildFullPos(pos, fullpos)
    ClkMtx = pos
    if cv2.waitKey(1) & 0xFF == ord('q'):
        scipy.io.savemat('D:\\Downloads\\zomerbaantje\\start project\\ImageResearch\\fullpos.mat', mdict = {'fullpos':fullpos})
        break

#steps:
#ask what frame to use? a way to check frames
#save the click coordinates in an array
#save the blobs in an array
#minimal distance choices
#show the combinations (blob connects to clicked marker since you know what click is what marker